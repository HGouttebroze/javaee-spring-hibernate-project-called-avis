package fr.humanbooster.hugues.avis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Joueur;
import fr.humanbooster.hugues.avis.dao.JoueurDao;
import fr.humanbooster.hugues.avis.service.JoueurService;

@Service
public class JoueurServiceImpl implements JoueurService {
	
	@Autowired
	private JoueurDao joueurDao;
	
	public JoueurServiceImpl(JoueurDao joueurDao) {
		super();
		this.joueurDao = joueurDao;
	}

	@Override
	public Joueur ajouterJoueur(String pseudo) {
		// TODO Auto-generated method stub
		return joueurDao.findByPseudo(pseudo);
	}

	@Override
	// TO DO: on a besoin de lui passer 2 params & là, j'en ai pas mis (voir methode...)
	public List<Joueur> recupererJoueur() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Joueur ajouterJoueur(Joueur joueur) {
		// TODO Auto-generated method stub
		return joueurDao.save(joueur);
	}

	@Override
	public List<Joueur> recupererJoueurParPseudoEtMotDePasse(String pseudo, String motDePasse) {
		// TODO Auto-generated method stub
		return joueurDao.findFirstByPseudoAndMotDePasse(pseudo, motDePasse);
	}

//	@Override
//	public List<Joueur> recupererJoueurParPseudoEtMotDePasse(String pseudo, String motDePasse) {
//		// TODO Auto-generated method stub
//		return joueurDao.findByPseudoEtMotDePasse(pseudo, motDePasse);
//	}



	

}
