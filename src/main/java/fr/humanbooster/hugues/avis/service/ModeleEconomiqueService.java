package fr.humanbooster.hugues.avis.service;

import java.util.List;

import fr.humanbooster.hugues.avis.business.ModeleEconomique;

public interface ModeleEconomiqueService {
	
	ModeleEconomique ajouterModeleEconomique(String nom);
	
	List<ModeleEconomique> recupererModeleEconomique();


}
