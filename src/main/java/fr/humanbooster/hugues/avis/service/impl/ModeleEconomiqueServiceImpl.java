package fr.humanbooster.hugues.avis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.ModeleEconomique;
import fr.humanbooster.hugues.avis.dao.ModeleEconomiqueDao;
import fr.humanbooster.hugues.avis.service.ModeleEconomiqueService;

@Service
public class ModeleEconomiqueServiceImpl implements ModeleEconomiqueService {

	@Autowired
	private ModeleEconomiqueDao modeleEconomiqueDao;
	
	

	public ModeleEconomiqueServiceImpl(ModeleEconomiqueDao modeleEconomiqueDao) {
		super();
		this.modeleEconomiqueDao = modeleEconomiqueDao;
	}





	@Override
	public ModeleEconomique ajouterModeleEconomique(String nom) {
		// TODO Auto-generated method stub
		return modeleEconomiqueDao.save(new ModeleEconomique());
	}





	@Override
	public List<ModeleEconomique> recupererModeleEconomique() {
		// TODO Auto-generated method stub
		return modeleEconomiqueDao.findAll();
	}


	





	

}
