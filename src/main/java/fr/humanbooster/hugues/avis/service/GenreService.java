package fr.humanbooster.hugues.avis.service;

import java.util.List;

import fr.humanbooster.hugues.avis.business.Genre;

public interface GenreService {
	
	Genre ajouterGenre(String nom);

	Genre recupererGenreFps(String string);
	
	List<Genre> recupererGenres();
	//List(Genre) recupererGenre(String nom);

}

