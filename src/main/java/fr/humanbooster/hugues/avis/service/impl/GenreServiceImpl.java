package fr.humanbooster.hugues.avis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Genre;
import fr.humanbooster.hugues.avis.dao.GenreDao;
import fr.humanbooster.hugues.avis.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService {
	
	@Autowired
	private GenreDao genreDao;
	
	

	public GenreServiceImpl(GenreDao genreDao) {
		super();
		this.genreDao = genreDao;
	}



	@Override
	public Genre ajouterGenre(String nom) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Genre recupererGenreFps(String string) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Genre> recupererGenres() {
		// TODO Auto-generated method stub
		return genreDao.findAll();
	}



//	@Override
//	public Genre recupererGenreFps(String string) {
//		// TODO Auto-generated method stub
//		return genreDao.findFirstByNomLike(string);
//	}

	



}

