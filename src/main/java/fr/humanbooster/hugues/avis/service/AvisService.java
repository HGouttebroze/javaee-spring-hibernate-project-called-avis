package fr.humanbooster.hugues.avis.service;

import java.util.Date;
import java.util.List;

import fr.humanbooster.hugues.avis.business.Avis;
import fr.humanbooster.hugues.avis.business.Jeu;
import fr.humanbooster.hugues.avis.business.Joueur;

public interface AvisService {
	
	Avis ajouterAvis();
	
	List<Avis> recupererAvis();

	Avis ajouterAvis(float note);
	
	Avis ajouterAvis(Avis avis);

	Avis ajouterAvis(Date date, float note, String description, List<Jeu> recupererJeux, Joueur joueurSession);

}
