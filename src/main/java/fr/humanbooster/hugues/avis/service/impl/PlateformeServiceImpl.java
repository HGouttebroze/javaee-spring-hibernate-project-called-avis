package fr.humanbooster.hugues.avis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Plateforme;
import fr.humanbooster.hugues.avis.dao.PlateformeDao;
import fr.humanbooster.hugues.avis.service.PlateformeService;

@Service
public class PlateformeServiceImpl implements PlateformeService {
	
	@Autowired
	private PlateformeDao plateformeDao;

	public PlateformeServiceImpl(PlateformeDao plateformeDao) {
		super();
		this.plateformeDao = plateformeDao;
	}

	@Override
	public Plateforme ajouterPlateforme(String nom) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Plateforme> recupererPlateforme() {
		// TODO Auto-generated method stub
		return plateformeDao.findAll();
	}

	
	
	

}
