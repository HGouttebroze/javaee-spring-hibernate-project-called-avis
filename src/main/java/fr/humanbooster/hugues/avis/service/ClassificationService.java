package fr.humanbooster.hugues.avis.service;

import java.util.List;

import fr.humanbooster.hugues.avis.business.Classification;

public interface ClassificationService {

	Classification ajouterClassification(String nom);
	
	List<Classification> recupererClassification();

	
}
