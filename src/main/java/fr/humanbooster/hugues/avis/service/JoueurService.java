package fr.humanbooster.hugues.avis.service;

import java.util.List;

import fr.humanbooster.hugues.avis.business.Joueur;

public interface JoueurService {
	
	Joueur ajouterJoueur(String pseudo);
	
	Joueur ajouterJoueur(Joueur joueur);
	
	List<Joueur> recupererJoueur();

	List<Joueur> recupererJoueurParPseudoEtMotDePasse(String pseudo, String motDePasse);
	

}
