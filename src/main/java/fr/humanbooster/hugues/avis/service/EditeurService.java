package fr.humanbooster.hugues.avis.service;

import java.util.List;

import fr.humanbooster.hugues.avis.business.Editeur;

public interface EditeurService {

	Editeur ajouterEditeur(String nom);
	List<Editeur> recupererEditeurs(String nom);
	List<Editeur> recupererEditeurs();
	
}
