package fr.humanbooster.hugues.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Editeur;
import fr.humanbooster.hugues.avis.dao.EditeurDao;
import fr.humanbooster.hugues.avis.service.EditeurService;

@Service
public class EditeurServiceImpl implements EditeurService {
	
	@Autowired
	private EditeurDao editeurDao;
	
	public EditeurServiceImpl(EditeurDao editeurDao) {
		super();
		this.editeurDao = editeurDao;
	}

	@Override
	public Editeur ajouterEditeur(String nom) {
		// TODO Auto-generated method stub
		return editeurDao.save(new Editeur(nom));
	}

	@Override
	public List<Editeur> recupererEditeurs(String nom) {
		// TODO Auto-generated method stub
		return editeurDao.findByNomLike(nom);
	}

	@Override
	public List<Editeur> recupererEditeurs() {
		// TODO Auto-generated method stub
		return editeurDao.findAll();
	}

	
	

}
