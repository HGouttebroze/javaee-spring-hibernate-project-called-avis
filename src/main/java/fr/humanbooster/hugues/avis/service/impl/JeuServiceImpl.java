package fr.humanbooster.hugues.avis.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Classification;
import fr.humanbooster.hugues.avis.business.Editeur;
import fr.humanbooster.hugues.avis.business.Genre;
import fr.humanbooster.hugues.avis.business.Jeu;
import fr.humanbooster.hugues.avis.business.ModeleEconomique;
import fr.humanbooster.hugues.avis.business.Plateforme;
import fr.humanbooster.hugues.avis.dao.GenreDao;
import fr.humanbooster.hugues.avis.dao.JeuDao;
import fr.humanbooster.hugues.avis.service.JeuService;

@Service
public class JeuServiceImpl implements JeuService {

	@Autowired
	private JeuDao jeuDao;
	private GenreDao genreDao;

	

public JeuServiceImpl(JeuDao jeuDao, GenreDao genreDao) {
		super();
		this.jeuDao = jeuDao;
		this.genreDao = genreDao;
	}

//	@Override
//	public List<Jeu> recupererJeux() {
//		// TODO Auto-generated method stub
//		return jeuDao.findAll();
//	}

	@Override
	public List<Jeu> recupererJeux(Genre genre) {
		// TODO Auto-generated method stub
		return jeuDao.findByGenre(genre);
	}

	@Override
	public List<Jeu> recupererJeux(Editeur editeur) {
		return jeuDao.findByEditeur(editeur);
	}

	@Override
	public List<Jeu> recupererJeuParNomDuGenre(String nom) {
		// TODO Auto-generated method stub
		return jeuDao.findByGenreNomLike(nom);
	}

	@Override
	public List<Jeu> recupererJeux() {
		// TODO Auto-generated method stub
		return jeuDao.findAll();
	}

	@Override
	public List<Jeu> recupererJeuParNomDuGenre(Genre genre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Jeu> recupererJeux(Date dateDebut, Date dateFin) {
		// TODO Auto-generated method stub
		return jeuDao.findByDateSortieBetween(dateDebut, dateFin);
	}

//	@Override
//	public Jeu ajouterJeu(String nom, String description, Date dateSortie, Editeur editeur,
//			ModeleEconomique modeleEconomique, Classification classification, Genre genre) {
//		// TODO Auto-generated method stub
//		return jeuDao.save(nom, description, dateSortie, editeur, modeleEconomique, classification, genre);
//	}

//	@Override
//	public Jeu ajouterJeuJeu(Jeu jeu) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Jeu> recupererJeuEditeur(Editeur editeur) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public List<Jeu> recupererJeuGenre(Genre genre) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Jeu recupererJeux(Long id) {
		return jeuDao.getOne(id);
	}

	 @Override
	    public Page<Jeu> recupererJeu(Long idGenre, Pageable pageable) {
	        //On fait appel à genreDao pour récuperer le genre dont l'id est donné en paramètre
//	        Genre genre = genreDao.getOne(idGenre);
	        return jeuDao.findByGenreId(idGenre,pageable);        
	    }

	@Override
	public Page<Jeu> recupererJeux(Pageable pageable) {
		// TODO Auto-generated method stub
		return jeuDao.findAll(pageable);
	}
	
	@Override
    public Jeu ajouterJeu(Jeu jeu) {
        return jeuDao.save(jeu);
    }

//	@Override
//	public Optional<Jeu> recupererJeux(Long id) {
//		// TODO Auto-generated method stub
//		return jeuDao.findById(id);
//	}

	@Override
    public boolean supprimerJeu(Long id) {
        
        jeuDao.deleteById(id);
        
        return recupererJeu(id) != null;
    }

	private Object recupererJeu(Long id) {
		// TODO Auto-generated method stub
		return jeuDao.findById(id);
	}




//	@Override
//	public Jeu supprimerJeu(Long id) {
//		// TODO Auto-generated method stub
//		return deleteById(id);
//	}

//	@Override
//	public Jeu ajouterJeu(String nom, String description, Date dateSortie, List<Plateforme> plateformes,
//			Editeur editeur, ModeleEconomique modeleEconomique, Classification classification, Genre genre) {
//		// TODO Auto-generated method stub
//		return jeuDao.save(nom, description, dateSortie, plateformes,editeur, modeleEconomique, classification, genre);
//	}

//	@Override
//	public void ajouterJeu(@Valid Jeu jeu) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public Jeu ajouterJeu(Jeu jeu) {
//		// TODO Auto-generated method stub
//		return jeuDao.save(jeu);
//	}


//	@Override
//	public List<Jeu> findByGenreNomLike(String nom) {
//		// TODO Auto-generated method stub
//		return jeuDao.findByGenre(null);
//	}

//	@Override
//	public Jeu ajouterJeu(String nom, String description, Date dateDeNaissance, Editeur editeur, Genre genre,
//			ModeleEconomique modeleEconomique, Classification classification, List<Plateforme> plateformes) {
//		// TODO Auto-generated method stub
//		return jeuDao.save(new Jeu(nom, description, dateDeNaissance));
//	}

}
