package fr.humanbooster.hugues.avis.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.hugues.avis.business.Classification;
import fr.humanbooster.hugues.avis.business.Editeur;
import fr.humanbooster.hugues.avis.business.Genre;
import fr.humanbooster.hugues.avis.business.Jeu;
import fr.humanbooster.hugues.avis.business.ModeleEconomique;
import fr.humanbooster.hugues.avis.business.Plateforme;

public interface JeuService {
	//Jeu ajouterJeu(String nom, String description, Date dateDeNaissance, Editeur editeur, Genre genre, ModeleEconomique modeleEconomique, Classification classification, List<Plateforme> plateformes);
	//Jeu ajouterJeuMini (String nom, String description, Date sortie);
	
	
	List<Jeu> recupererJeux();
	
	List<Jeu> recupererJeux(Genre genre);
	List<Jeu> recupererJeux(Editeur editeur);
	

	List<Jeu> recupererJeuParNomDuGenre(String nom);

	List<Jeu> recupererJeuParNomDuGenre(Genre genre);
	
	List<Jeu> recupererJeux(Date dateDebut, Date dateFin); 
	
	Page<Jeu> recupererJeux(Pageable pageable);
	
//	Jeu ajouterJeu(String nom, String description, Date dateSortie, List<Plateforme> plateformes ,Editeur editeur,
//           ModeleEconomique modeleEconomique, Classification classification, Genre genre);

    Jeu ajouterJeu(Jeu jeu);
//   
//    List<Jeu> recupererJeuEditeur(Editeur editeur); 
//    
//    List<Jeu> recupererJeuGenre(Genre genre);
    
    //Dès qu'une méthode à en paramètre un pageable elle doit renvoyer une page.
    Page<Jeu> recupererJeu(Long idGenre, Pageable pageable);
    
    Jeu recupererJeux(Long id);

	boolean supprimerJeu(Long id);

	//Jeu supprimerJeu(Long id);

	//void ajouterJeu(@Valid Jeu jeu);
}
