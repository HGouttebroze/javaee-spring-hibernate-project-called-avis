package fr.humanbooster.hugues.avis.service;

import java.util.List;

import fr.humanbooster.hugues.avis.business.Plateforme;

public interface PlateformeService {
	
	Plateforme ajouterPlateforme(String nom);
	
	List<Plateforme> recupererPlateforme();
	
	

}
