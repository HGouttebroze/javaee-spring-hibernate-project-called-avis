package fr.humanbooster.hugues.avis.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Avis;
import fr.humanbooster.hugues.avis.business.Jeu;
import fr.humanbooster.hugues.avis.business.Joueur;
import fr.humanbooster.hugues.avis.dao.AvisDao;
import fr.humanbooster.hugues.avis.service.AvisService;

@Service
public class AvisServiceImpl implements AvisService {
	
	@Autowired
	private AvisDao avisDao;
	
	public AvisServiceImpl(AvisDao avisDao) {
		super();
		this.avisDao = avisDao;
	}

	@Override
	public Avis ajouterAvis(float note) {
		// TODO Auto-generated method stub
		return avisDao.findByNote(note);
	}

	@Override
	public Avis ajouterAvis() {
		// TODO Auto-generated method stub
		return avisDao.save(new Avis());
	}
	@Override
	public List<Avis> recupererAvis() {
		// TODO Auto-generated method stub
		return avisDao.findAll();
	}

	@Override
	public Avis ajouterAvis(Avis avis) {
		// TODO Auto-generated method stub
		return avisDao.save(avis);
	}

	@Override
	public Avis ajouterAvis(Date date, float note, String description, List<Jeu> recupererJeux, Joueur joueurSession) {
		// TODO Auto-generated method stub
		return null;
	}


}
