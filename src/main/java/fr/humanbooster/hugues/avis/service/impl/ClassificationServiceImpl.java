package fr.humanbooster.hugues.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.humanbooster.hugues.avis.business.Classification;
import fr.humanbooster.hugues.avis.dao.ClassificationDao;
import fr.humanbooster.hugues.avis.service.ClassificationService;

@Service
public class ClassificationServiceImpl implements ClassificationService {
	
	@Autowired
	private ClassificationDao classificationDao;
	
	

	public ClassificationServiceImpl(ClassificationDao classificationDao) {
		super();
		this.classificationDao = classificationDao;
	}



	@Override
	public Classification ajouterClassification(String nom) {
		// TODO Auto-generated method stub
		return classificationDao.save(new Classification());
	}



	@Override
	public List<Classification> recupererClassification() {
		// TODO Auto-generated method stub
		return classificationDao.findAll();
	}



	







}
