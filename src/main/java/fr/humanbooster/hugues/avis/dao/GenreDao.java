package fr.humanbooster.hugues.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.hugues.avis.business.Genre;

public interface GenreDao extends JpaRepository<Genre, Long> {
	
		// A TESTER:
		//Genre findByNomStartingWith(String nom);

	    Genre findFirstByNomLike(String nom);
	    
	    Genre findByNom(String nom);

		//Genre findAll(String nom);
	    
	    // distinct: pr eviter les doublons! (voir aussi les Set, comme sur png pris sur code de Max)
	    // Genre findDistinct(String nom);
	    
	    

}
