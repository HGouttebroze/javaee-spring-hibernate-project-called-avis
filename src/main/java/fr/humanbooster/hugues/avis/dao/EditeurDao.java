package fr.humanbooster.hugues.avis.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.humanbooster.hugues.avis.business.Editeur;

public interface EditeurDao extends JpaRepository<Editeur, Long> {
	
	List<Editeur> findByNomLike(String nom);
	
	// le @Query prend le dessus sur la méthode "findDistinctByJeuxGenreNomStartingWith"
	@Query("select distinct j.editeur from Jeu j where j.genre.nom Like ?1%")
	// la méthode ci-dessous a 1 nom qui va juste ns donner 1 indication sur sa fonction
    List<Editeur> findDistinctByJeuxGenreNomStartingWith(String nom);
	

}


