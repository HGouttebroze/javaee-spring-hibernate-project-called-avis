package fr.humanbooster.hugues.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.hugues.avis.business.Classification;

public interface ClassificationDao extends JpaRepository<Classification, Long> {

}
