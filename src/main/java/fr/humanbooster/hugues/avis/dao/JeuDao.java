package fr.humanbooster.hugues.avis.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.humanbooster.hugues.avis.business.Editeur;
import fr.humanbooster.hugues.avis.business.Genre;
import fr.humanbooster.hugues.avis.business.Jeu;

public interface JeuDao extends JpaRepository<Jeu, Long> {
	
	List<Jeu> findByGenre(Genre genre);

	List<Jeu> findByEditeur(Editeur editeur);

	List<Jeu> findByEditeurAndGenre(Editeur editeur, Genre genre);

	List<Jeu> findByNomLike(String nom);

	List<Jeu> findByNomLikeAndDateSortieBetween(String nom, Date dateDebut, Date dateFin);

	List<Jeu> findByEditeurAndNomLikeAndDateSortieBetween(Editeur editeur, String nom, Date dateDebut, Date dateFin);
	
	List<Jeu> findByGenreNomLike(String nom);
	
	Page<Jeu> findByGenreId(Long idGenre, Pageable pageable);
	
	
	
	//List<Jeu> findJeuByPlateforme(String nom);      
	
	// @Query: Déclare la requête HQL associée à la méthode de l’interface de DAO
	@Query("select distinct j.editeur from Jeu j where j.genre.nom Like ?1%")
    List<Editeur> findDistinctByJeuxGenreNomStartingWith(String nom); 
	
	// @Param: Déclare les paramètres de la méthode Java devant être utilisés comme des paramètres HQL
	@Query("from Jeu where dateSortie between :dateDebut and :dateFin")
    List<Jeu> findByDateSortieBetween(@Param("dateDebut") Date dateDebut, @Param("dateFin") Date dateFin);
}
