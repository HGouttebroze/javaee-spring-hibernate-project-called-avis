package fr.humanbooster.hugues.avis.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import fr.humanbooster.hugues.avis.business.Joueur;

public interface JoueurDao extends JpaRepository<Joueur, Long> {
	
	Joueur findByPseudo(String pseudo);
	
	Joueur findByMotDePasse(String motDePasse);
	
	List<Joueur> findFirstByPseudoAndMotDePasse(String pseudo, String motDePasse); 
	
	//List<Joueur> findByPseudoEtMotDePasse(String pseudo, String motDePasse);

}
