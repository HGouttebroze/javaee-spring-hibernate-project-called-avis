package fr.humanbooster.hugues.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.hugues.avis.business.Plateforme;

public interface PlateformeDao extends JpaRepository<Plateforme, Long> {

}
