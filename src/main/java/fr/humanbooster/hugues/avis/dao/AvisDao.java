package fr.humanbooster.hugues.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.hugues.avis.business.Avis;

public interface AvisDao extends JpaRepository<Avis, Long> {

	Avis findByNote(float note);
	
}
