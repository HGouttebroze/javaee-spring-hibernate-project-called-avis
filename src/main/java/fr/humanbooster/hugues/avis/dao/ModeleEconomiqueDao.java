package fr.humanbooster.hugues.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.hugues.avis.business.ModeleEconomique;

public interface ModeleEconomiqueDao extends JpaRepository<ModeleEconomique, Long> {

	ModeleEconomique findByNom(String nom);
}
