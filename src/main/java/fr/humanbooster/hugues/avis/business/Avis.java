package fr.humanbooster.hugues.avis.business;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Avis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date dateEnvoi;
	private float note;
	private String description;
	@ManyToOne
	private Jeu avisJeu;
	@ManyToOne
	private Joueur joueur;
	public Avis() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDateEnvoi() {
		return dateEnvoi;
	}
	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}
	public float getNote() {
		return note;
	}
	public void setNote(float note) {
		this.note = note;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Jeu getAvisJeu() {
		return avisJeu;
	}
	public void setAvisJeu(Jeu avisJeu) {
		this.avisJeu = avisJeu;
	}
	public Joueur getJoueur() {
		return joueur;
	}
	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}
	@Override
	public String toString() {
		return "Avis [id=" + id + ", dateEnvoi=" + dateEnvoi + ", note=" + note + ", description=" + description
				+ ", avisJeu=" + avisJeu + ", joueur=" + joueur + "]";
	}
	
	
	
}
	