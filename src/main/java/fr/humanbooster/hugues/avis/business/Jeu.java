package fr.humanbooster.hugues.avis.business;

import java.beans.Transient;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;


@Entity
public class Jeu {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String description;
	private Date dateSortie;
	@ManyToOne
	private Editeur editeur;
	@ManyToOne
	private ModeleEconomique modeleEconomique;
	@ManyToOne
	private Classification classification;
	@ManyToOne
	private Genre genre;
	@OneToMany(mappedBy = "avisJeu")
	private List<Avis> avis;  
	
	//@JoinTable(name="jeu_plateformes")
	@ManyToMany
	private List<Plateforme> plateformeJeu;
	
	// upload img
//	@Column(nullable = true, length = 64)
	private String image;
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Jeu() {
		
	}
	public Jeu(String nom2, String description2, Date dateSortie2) {
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateSortie() {
		return dateSortie;
	}
	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}
	public Editeur getEditeur() {
		return editeur;
	}
	public void setEditeur(Editeur editeur) {
		this.editeur = editeur;
	}
	public ModeleEconomique getModeleEconomique() {
		return modeleEconomique;
	}
	public void setModeleEconomique(ModeleEconomique modeleEconomique) {
		this.modeleEconomique = modeleEconomique;
	}
	public Classification getClassification() {
		return classification;
	}
	public void setClassification(Classification classification) {
		this.classification = classification;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	public List<Avis> getAvis() {
		return avis;
	}
	public void setAvis(List<Avis> avis) {
		this.avis = avis;
	}
	public List<Plateforme> getPlateformeJeu() {
		return plateformeJeu;
	}
	public void setPlateformeJeu(List<Plateforme> plateformeJeu) {
		this.plateformeJeu = plateformeJeu;
	}
	
	public @Valid Jeu ajouterJeu(@Valid Jeu jeu) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transient
    public String getCheminImage() {
        if (image == null || id==null) return null;
        return "jeu-images/" + id + "/" + image;
    }
	
	@Override
	public String toString() {
		return "Jeu [id=" + id + ", nom=" + nom + ", description=" + description + ", dateSortie=" + dateSortie
				+ ", editeur=" + editeur + ", modeleEconomique=" + modeleEconomique + ", classification="
				+ classification + ", genre=" + genre + "]";
	}
	
	
	
}
