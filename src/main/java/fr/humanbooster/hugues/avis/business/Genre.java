package fr.humanbooster.hugues.avis.business;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
public class Genre {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message="Veuillez renseigner le libellé du genre")
    private String nom;
    @NotEmpty(message = "Merci de préciser les jeux concernés")
    @OneToMany(mappedBy="genre")
    private List<Jeu> jeux;
	public Genre() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<Jeu> getJeux() {
		return jeux;
	}
	public void setJeux(List<Jeu> jeux) {
		this.jeux = jeux;
	}
	@Override
	public String toString() {
		return "Genre [id=" + id + ", nom=" + nom + "]";
	}
    
    
}
