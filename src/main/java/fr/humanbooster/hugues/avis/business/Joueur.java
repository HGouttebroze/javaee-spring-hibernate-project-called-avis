package fr.humanbooster.hugues.avis.business;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;

@Entity
public class Joueur {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Pattern(regexp="^[A-Za-z]+$", message="Le pseudo doit contenir uniquement des lettres")
	private String pseudo;
	private String motDePasse;
	@Temporal(TemporalType.DATE)
	private Date dateInscription;
	private Boolean estAdministrateur;
	@OneToMany(mappedBy="joueur")
	private List<Avis> avis;
	public Joueur() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}
	public Boolean getEstAdministrateur() {
		return estAdministrateur;
	}
	public void setEstAdministrateur(Boolean estAdministrateur) {
		this.estAdministrateur = estAdministrateur;
	}
	public List<Avis> getAvis() {
		return avis;
	}
	public void setAvis(List<Avis> avis) {
		this.avis = avis;
	}
	@Override
	public String toString() {
		return "Joueur [id=" + id + ", pseudo=" + pseudo + ", motDePasse=" + motDePasse + ", dateInscription="
				+ dateInscription + ", estAdministrateur=" + estAdministrateur + "]";
	}
	
	
	
}
