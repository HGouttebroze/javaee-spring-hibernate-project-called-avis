package fr.humanbooster.hugues.avis.controlleur;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import fr.humanbooster.hugues.avis.business.Avis;
import fr.humanbooster.hugues.avis.business.Editeur;
import fr.humanbooster.hugues.avis.business.Jeu;
import fr.humanbooster.hugues.avis.business.Joueur;
import fr.humanbooster.hugues.avis.service.AvisService;
import fr.humanbooster.hugues.avis.service.ClassificationService;
import fr.humanbooster.hugues.avis.service.EditeurService;
import fr.humanbooster.hugues.avis.service.GenreService;
import fr.humanbooster.hugues.avis.service.JeuService;
import fr.humanbooster.hugues.avis.service.JoueurService;
import fr.humanbooster.hugues.avis.service.ModeleEconomiqueService;
import fr.humanbooster.hugues.avis.service.PlateformeService;

@Controller
public class AvisControlleur { 
    private GenreService genreService;
    private ClassificationService classificationService;
    private EditeurService editeurService;
    private JeuService jeuService;
    private AvisService avisService;
    private PlateformeService plateformeService; 
    // on declare l'oblet HttpSession & on le met ds le controleur
    private HttpSession httpSession;
    private ModeleEconomiqueService modeleEconomiqueService;
    private JoueurService joueurService;
    
    // Constante pour l'upload d'images
    private static final String DOSSIER_IMAGES = "src/main/webapp/jeu-images/";
   
    // Spring va injecter les objet nécessaire au controlleur (la session est injecter par TomCat et non par Spring)
    

	// l'annotation @RequestMapping est l'équivalent de l'annotation @WebServlet
    // cette méthode prend encharge 2 URL: "/index" & "/"
    // la liste des jeux s'affiche alors sur le localhost (avec "index.jsp")
//    @RequestMapping(value = { "/index", "/" })
//    public ModelAndView accueil() {    	
//    	// on declare 1 objet de type ModeleAndView, on l'instencie (c'est une danette)
//        ModelAndView mav = new ModelAndView();
//        // on renseigne la vue associé au modèleAndView
//        // la vue est "index.jsp", grâce au file Aplication.properties (ligne 13), il n'est pas nécessaire de mettre le suffixe ".jsp"
//        mav.setViewName("index");
//        // on ajoute ds la danette, ds le conpartiment à petite bille, 1 petite bille correspondand à la liste des jeux stockés en base !!!
//        mav.addObject("jeux", jeuService.recupererJeux());
//        // on revoie l' objet ModelAldView à la vue  
//        return mav;
//    }  
        

       
    public AvisControlleur(GenreService genreService, ClassificationService classificationService,
			EditeurService editeurService, JeuService jeuService, AvisService avisService,
			PlateformeService plateformeService, HttpSession httpSession,
			ModeleEconomiqueService modeleEconomiqueService, JoueurService joueurService) {
		super();
		this.genreService = genreService;
		this.classificationService = classificationService;
		this.editeurService = editeurService;
		this.jeuService = jeuService;
		this.avisService = avisService;
		this.plateformeService = plateformeService;
		this.httpSession = httpSession;
		this.modeleEconomiqueService = modeleEconomiqueService;
		this.joueurService = joueurService;
	}


	@RequestMapping(value = { "/editeurs" })
    public ModelAndView editeur() {
    	// on declare 1 objet de type ModeleAndView, on l'instencie (c'est une danette)
        ModelAndView mav = new ModelAndView();
        // on renseigne la vue associé au modèleAndView
        // la vue est "index.jsp", grâce au file Aplication.properties (ligne 13), il n'est pas nécessaire de mettre le suffixe ".jsp"
        mav.setViewName("editeurs");
        // on ajoute ds la danette, ds le conpartiment à petite bille, 1 petite bille correspondand à la liste des jeux stockés en base !!!
        mav.addObject("editeurs", editeurService.recupererEditeurs());
        // on revoie l' objet ModelAldView à la vue 
        return mav;
    }
    
   
	@RequestMapping(value = { "/index", "/" })
    public ModelAndView accueil(@PageableDefault(value = 10, sort =
            "nom", direction = Direction.ASC ) Pageable pageable) {
        //On déclare un objet de type ModeleAndView et on l'instancie
        ModelAndView mav = new ModelAndView();
        //On renseigne la vue associé au ModelAndView
        //La vue est index.jsp, grâce au fichier application.properties (ligne 13) 
        //il n'est pas nécessaire de préciser l'extension .jsp
        mav.setViewName("index");
        //On ajoute dans la danette dans le compartiment une petite bille (Objet) stocké en base.
        mav.addObject("jeux", jeuService.recupererJeux(pageable));
        mav.addObject("genres", genreService.recupererGenres());
        //On renvoie l'objet ModelAndView mav
        return mav;
    }
    
    @PostMapping("filtrer")
    public ModelAndView filtrer(@RequestParam("ID_GENRE") Long idGenre,@PageableDefault(value = 10, sort =
            "nom", direction = Direction.ASC ) Pageable pageable) {
        ModelAndView mav = new ModelAndView("index");
        if (idGenre.equals(0L)) {
            mav.addObject("pageDeJeux", jeuService.recupererJeux(pageable));

        }else {
              mav.addObject("pageDeJeux", jeuService.recupererJeu(idGenre,pageable));
        }
          mav.addObject("jeux", jeuService.recupererJeu(idGenre,pageable));
          mav.addObject("genres", genreService.recupererGenres());
          //On renvoie l'objet ModelAndView mav
          return mav; 
    }
    
//    @Scheduled(cron="* * * * * *")
//    public void ajouterJoueur() {
//        Date date = new Date();
//        Date dateNaissance = new Date(date.getTime() - 10);
//        //byte[] array = new byte[7]; // length is bounded by 7
//        //new Random().nextBytes(array);
//        //String generatedString = new String(array, Charset.forName("UTF-8"));
//        joueurService.ajouterJoueur("pilou", "1234", dateNaissance, false);
//
//    }

    

    
    @GetMapping(value = { "/jeu" })
    public ModelAndView jeuGet(@RequestParam(name="id", required = false) Long id) {
        ModelAndView mav = new ModelAndView("jeu");
        
        if (id == null) {
            mav.addObject("jeu", new Jeu());
            
        } else {
            mav.addObject("jeu", jeuService.recupererJeux(id));
        }
        
        mav.addObject("editeurs", editeurService.recupererEditeurs());
        mav.addObject("genres", genreService.recupererGenres());
        mav.addObject("classifications", classificationService.recupererClassification());
        //mav.addObject("plateformes", plateformeService.recupererPlateforme());
        mav.addObject("modeleEconomiques", modeleEconomiqueService.recupererModeleEconomique());

        return mav; 
    } 

    @PostMapping("jeu")
    public ModelAndView jeuPost(@Valid @ModelAttribute Jeu jeu, BindingResult result) {

        if (result.hasErrors()) {

            ModelAndView mav = jeuGet(jeu.getId());
            mav.addObject("jeu", jeu);
            return mav;

        } else {

            jeuService.ajouterJeu(jeu);
            return new ModelAndView("redirect:index");
        }
    }
    
    @GetMapping("image")
    public ModelAndView imageGet(@RequestParam("id") Long id) {
        ModelAndView mav = new ModelAndView("image");
        
        mav.addObject("jeu", jeuService.recupererJeux(id));

        return mav;
    }
    
    @PostMapping("image")
    public ModelAndView imagePost(@RequestParam("id") Long id, @RequestParam("image") MultipartFile multipartFile) throws IOException {
        
        Jeu jeu = jeuService.recupererJeux(id);
        
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        jeu.setImage(fileName);
        
        jeuService.ajouterJeu(jeu);
        
        String uploadDir = DOSSIER_IMAGES + jeu.getId();
        
        saveFile(uploadDir, fileName, multipartFile);
        
        return new ModelAndView("redirect:index");
    }
    
    private static void saveFile(String uploadDir, String fileName,
            MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);
         
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
         
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {        
            throw new IOException("Could not save image file: " + fileName, ioe);
        }      
    }

// En cliquant sur un lien HTTP, une requête dont la méthode est GET est envoyée au serveur
    @GetMapping("supprimer")
    public ModelAndView supprimerJeu(@RequestParam("id") Long id) {
        
        jeuService.supprimerJeu(id);
        return new ModelAndView("redirect:index");
    }
    
    @PostMapping(value = "/supprimer")
    public ModelAndView supprimerGet(@RequestParam("ID_JEU") Long id) {
//        for (Jeu jeu : jeuService.recupererJeu(id)) {
//            jeuService.supprimer(jeux);
//        }
        jeuService.supprimerJeu(id);
        ModelAndView mav = new ModelAndView("redirect:index");
        return mav;
    }
    
    @GetMapping("/inscription")
	public ModelAndView inscriptionGet() {
		ModelAndView mav = new ModelAndView("inscription");
		mav.addObject("joueur", new Joueur());
		return mav;
	}
    
    @PostMapping("/inscription")
	public ModelAndView inscriptionPost(@Valid @ModelAttribute Joueur joueur, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView mav = inscriptionGet();
			// le result ns dis si il y a des erreur
			System.out.println(result);
			mav.addObject("joueur", joueur);
			return mav;
		} else {
			// TODO vérifier mail pas déjà en base !
			joueur = joueurService.ajouterJoueur(joueur);
			ModelAndView mav = new ModelAndView("merciInscription");
			mav.addObject("joueur", joueur);
			return mav;
		}
	}
    
//    @PostMapping("/ajouterJeu")
//	public ModelAndView jeuPost(@Valid @ModelAttribute Jeu jeu, BindingResult result) {
//		if (result.hasErrors()) {
//			ModelAndView mav = inscriptionGet();
//			// le result ns dis si il y a des erreur
//			System.out.println(result);
//			mav.addObject("jeu", jeu);
//			return mav;
//		} else {
//			// TODO vérifier mail pas déjà en base !
//			jeu = jeu.ajouterJeu(jeu);
//			ModelAndView mav = new ModelAndView("redirect: index");
//			return mav;
//		}
//	}
   
//    Joueur joueur = joueurService.recupererJoueur(pseudo, motDePasse);
//    if(joueur != null) {
//    	httpSession.setAttribute("joueur", joueur);
//    	return accueil(pageable);
//    } else {
//    	ModelAndView mav = new ModelAndView("connexion");
//    	mav.addObject("notification", "Erreur de connexion");
//    	return mav;
//    }
//    
//    }
    

//	@GetMapping(value = { "/avis" })
//    public ModelAndView avis() {
//    	// on declare 1 objet de type ModeleAndView, on l'instencie (c'est une danette)
//        ModelAndView mav = new ModelAndView();
//        //Joueur joueurSession = (Joueur) httpSession.getAttribute("joueur");
//        // on renseigne la vue associé au modèleAndView
//        // la vue est "index.jsp", grâce au file Aplication.properties (ligne 13), il n'est pas nécessaire de mettre le suffixe ".jsp"
//        mav.setViewName("avis");
//        // on ajoute ds la danette, ds le compartiment à petite bille, 1 petite bille correspondand à la liste des jeux stockés en base !!!
//        mav.addObject("jeux", jeuService.recupererJeux());
//        mav.addObject("avis", avisService.recupererAvis()); 	
//        // on revoie l' objet ModelAldView à la vue 
//        return mav;
//        //return new ModelAndView("redirect:index");
//    }
    
    @GetMapping("/ajouterAvis")
    public ModelAndView ajouterAvisGet() {
    	Joueur joueurSession = (Joueur) httpSession.getAttribute("joueur");
    	if (joueurSession != null) {
    		
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("ajouterAvis");
    	mav.addObject("jeux", jeuService.recupererJeux());
    	return mav;
    	}else {
    		ModelAndView mavNew= new ModelAndView();
    		mavNew.setViewName("annotation");
    		mavNew.addObject("annotation", "attention: pas de joueur en session");
    		return mavNew;
    	}
    }
    
    @PostMapping("/ajouterAvis")
    //@Valid permet de délèguer à Spring le travail de validation sur l’objet annoté
       public ModelAndView avis(@RequestParam("ID_JEU") Long idJeu,
               @RequestParam("DESCRIPTION") String description, @RequestParam("ID_NOTE") float note) {
        //Récupération du joueur de la session
        Joueur joueurSession = (Joueur) httpSession.getAttribute("joueur");
        //Ajout de l'avis avec les différents services
        Avis avis = avisService.ajouterAvis(new Date(), note, description, jeuService.recupererJeux(), joueurSession);
        //Avis avis = avisService.ajouterAvis();
           if (avis == null) {
               return new ModelAndView("avis");
           } else {
               System.out.println("L'avis a été ajouté dans la base");
               return new ModelAndView("redirect:avis");
           } 
       }
    
    @GetMapping("connexion")
    public ModelAndView connectionGet() {
        return new ModelAndView("connexion"); 
    }

    @PostMapping("connexion")
     public ModelAndView connecterPost(@RequestParam("PSEUDO") String pseudo,@RequestParam("MOT_DE_PASSE") String motDePasse) {
         List<Joueur> joueur = joueurService.recupererJoueurParPseudoEtMotDePasse(pseudo, motDePasse);
         if (joueur != null) {
        	 return new ModelAndView("connexion");
//             httpSession.setAttribute("joueur", joueur);
//            return accueil(PageRequest.of(0, 10));
        } else {
        	httpSession.setAttribute("joueur", joueur); 
        	return new ModelAndView("redirect:index");
            //On redirige le joueur vers la page de connexion en le notifiant de l'erreur de connexion
//            ModelAndView mav = new ModelAndView("connexion");
//            mav.addObject("notification", "Erreur de connection");
//            return mav;
        }
     }
//	@PostMapping("/connexion")
//	public ModelAndView connexionPost(@RequestParam("pseudo") String pseudo,
//			@RequestParam("motDePasse") String motDePasse) {
//		Joueur joueur = joueurService.recupererJoueur(pseudo, motDePasse);
//		if (joueur == null) {
//			return new ModelAndView("connexion");
//		} else {
//			httpSession.setAttribute("joueur", joueur);
//			return new ModelAndView("redirect:index");
//		}
//	}
	
    
    @GetMapping("/deconnexion")
    public ModelAndView deconnexion() {
        httpSession.invalidate();
        return new ModelAndView("redirect:index");
    }

    @PostConstruct
    public void afficherDonnees() {
        System.out.println(jeuService.recupererJeux());
        
        System.out.println(editeurService.recupererEditeurs("Arts"));
        for (Editeur editeur : editeurService.recupererEditeurs("Arts")) {
            System.out.println(editeur.getNom());
            editeur.setJeux(jeuService.recupererJeux(editeur));
            for (Jeu jeu : editeur.getJeux()) {
                System.out.println(jeu.getNom());
            }
        }
    
     // Trie des Jeux sur les dates (entre 2011 & 2020)
     // on créé 1 méthode ds l'interface JeuDao (ici, on use @Query ou @Param !!!)
     // on créé le service `recupererJeu` puis on la créé ds l'implémentation du service Jeu
     Calendar c = Calendar.getInstance();
     c.set(Calendar.YEAR, 2011);
     Date dateDebut = c.getTime();
     c.set(Calendar.YEAR, 2020);
     Date dateFin = c.getTime();
     for (Jeu jeu : jeuService.recupererJeux(dateDebut, dateFin)) {
         System.out.println(jeu.getNom() + " " + jeu.getDateSortie());
}
   
//   @RequestMapping(value = { "/index", "/" })
//   public ModelAndView accueil(
//           @PageableDefault(size = 10, sort = "nom", direction = Sort.Direction.ASC) Pageable pageable) {
//       ModelAndView mav = new ModelAndView();
//       // On renseigne la vue associée au MOV
//       // La vue enst index.jsp, grace au fichier application.properties (ligne 13)
//       // Il n'est pas nécessaire de préciser l'extension (aussi dans
//       // application.properties)
//       mav.setViewName("index");
//       // On ajoute dans la danette dans le compartiment une pette bille (objet)
//       // stockée en base
//       // nb : le parametere pageable est transmis au service
//       mav.addObject("jeux", jeuService.recupererJeux(pageable));
//       // On retroune la vue
//       return mav;
//
//   }
   
//   @RequestMapping(value = { "/index", "/" })
//   public ModelAndView accueil(
//           @PageableDefault(size = 10, sort = "nom", direction = Sort.Direction.ASC) Pageable pageDeJeux) {
//       ModelAndView mav = new ModelAndView();
//       // On renseigne la vue associée au MOV
//       // La vue enst index.jsp, grace au fichier application.properties (ligne 13)
//       // Il n'est pas nécessaire de préciser l'extension (aussi dans
//       // application.properties)
//       mav.setViewName("index");
//       // On ajoute dans la danette dans le compartiment une pette bille (objet)
//       // stockée en base
//       // nb : le parametere pageable est transmis au service
//       mav.addObject("jeux", jeuService.recupererJeux(pageDeJeux));
//       // On retroune la vue
//       return mav;
//
//   }
//     @RequestMapping(value = { "/index", "/" })
//     public ModelAndView accueil() {
//         ModelAndView mav = new ModelAndView();
//         mav.setViewName("index");
//         mav.addObject("jeux", jeuService.recupererJeux());
//         return mav;
//     }
   
//   @GetMapping(value = {"jeu"})
//   public ModelAndView jeuGet(@RequestParam(required = false) Long idJeu) {
//       ModelAndView mav = new ModelAndView("jeu");
//       Jeu jeu;
//       if (idJeu == null) {
//           jeu = new Jeu();
//       }
//       else {
//           jeu = jeuService.recupererJeuParId(idJeu);
//       }
//       mav.addObject("jeu", jeu);
//       mav.addObject("editeurs", editeurService.recupererEditeurs());
//       mav.addObject("classifications", classificationService.recupererClassifications());
//       mav.addObject("plateformes", plateformeService.recupererPlateformes());
//       mav.addObject("modeleEconomiques", modeleEconomiqueService.recupererModeleEconomiques());
//       mav.addObject("genres", genreService.recuperergenres());
//       return mav;
//   }
//   
//       @GetMapping("/ajouterJeu")
//  	public ModelAndView ajouterJeuGet() { 
//   	//Joueur joueurSession = (Joueur) httpSession.getAttribute("joueur");
//   	// TO DO: gerer les session avec 1 if (voic capture écran "getmapping.png"
//  		ModelAndView mav = new ModelAndView("jeu"); 
//  		mav.setViewName("ajouterJeu");
//  		mav.addObject("jeu", new Jeu()); 
//  		mav.addObject("editeurs", editeurService.recupererEditeurs());
//       mav.addObject("genres", genreService.recupererGenres());
//       mav.addObject("classifications", classificationService.recupererClassification());
//      //mav.addObject("plateformes", plateformeService.recupererPlateforme()); 
//       mav.addObject("modeleEconomiques", modeleEconomiqueService.recupererModeleEconomique());
//  		return mav;
//  	}
   
       
//   @PostMapping("jeu")
//   public ModelAndView jeuPost(@Valid @ModelAttribute Jeu jeu, BindingResult result) {
//       if (result.hasErrors()) {
//           System.out.println(result);
//           ModelAndView mav = jeuGet(jeu.getId());
//           return mav;
//       } else {
//           jeu = jeuService.ajouterJeu(jeu);
//           ModelAndView mav = new ModelAndView("redirect:index");
//           return mav;
//       }
//   }
   
//   @PostMapping("/ajouterJeu")
//   public ModelAndView ajouterJeuPost(@Valid @ModelAttribute("jeu") Jeu jeu, BindingResult result) {
//       if (result.hasErrors()) {
//           ModelAndView mav = inscriptionGet();
//       	//ModelAndView mav = ajouterJeuPost(jeu.getId());
//           mav.addObject("jeu", jeu);
//           return mav;
//       } else {
//           jeuService.ajouterJeu(jeu);
//           System.out.println("Le jeu a été ajouté à la base de données");
//           return new ModelAndView("redirect: index");
//       }
//   }   
     
     
// -------------------------------------------------------------------------------
     // exemple avec 1 Set (évite les doublons)
     // To Do: créer la méthode ds la Dao (genreDao), puis service & serviceImpl
//        Set<Editeur> editeurs = new HashSet<>();
//        Genre genre = genreService.recupererGenre("rpg");
//        for (Jeu jeu : jeuService.recupererJeux(genre)) {
//            editeurs.add(jeu.getEditeur());
//        }
//        for (Editeur editeur : editeurs) {
//            System.out.println(editeur.getNom());
//        }

     
     
     
     
     
//        Genre genre = genreService.recupererGenreFps("rpg");
//        genre.setJeux(jeuService.recupererJeuParNomDuGenre(genre));
//        for (Jeu jeu : genre.getJeux()) {
//            System.out.println(jeu.getEditeur().getNom());
//        }
       
    }
    
   
    
//    System.out.println(editeurService.recupererEditeur("Arts"));
//    for (Editeur editeur : editeurService.recupererEditeur("Arts")) {
//        System.out.println(editeur.getNom());
//        for (Jeu jeu : editeur.getJeux()) {
//            System.out.println(jeu.getNom());
//        }
//    }
//    }
}

//	public AvisControlleur(EditeurService editeurService) {
//		super();
//		this.editeurService = editeurService;
//	}
	
//	@PostConstruct
//	public void ajouterDonneInitiale() {
//	    editeurService.ajouterEditeur("Riot games");
//		//editeurService.ajouterEditeur("Straight to hell's games");
//	}

//@PostConstruct
//public void afficherDonnee() {
////    List<Jeu> jeux = jeuService.recupererJeux();
////    for (Jeu jeu : jeux) {
////        System.out.println(jeu.getNom());
////    }
//    System.out.println(editeurService.recupererEditeurs("Arts"));
//    for (Editeur editeur : editeurService.recupererEditeurs("Arts")) {
//        System.out.println(editeur.getNom());
//        editeur.setJeux(jeuService.recupererJeux(editeur));
//        for (Jeu jeu : editeur.getJeux()) {
//            System.out.println(jeu.getNom());
//        }
//    }
//}

