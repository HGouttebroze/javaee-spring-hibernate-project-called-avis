<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<%-- <form:form method="POST" action="/spring-mvc-xml/uploadFile" enctype="multipart/form-data"> --%>
<!--     <table> -->
<!--         <tr> -->
<%--             <td><form:label path="file">Select a file to upload</form:label></td> --%>
<!--             <td><input type="file" name="file" /></td> -->
<!--         </tr> -->
<!--         <tr> -->
<!--             <td><input type="submit" value="Submit" /></td> -->
<!--         </tr> -->
<!--     </table> -->
<%-- </form:form> --%>

<form th:action="@{/jeu/save}"
    th:object="${jeu}" method="post"
    enctype="multipart/form-data"
    >
   
    <div>
     
    <label>Photos: </label>
    <input type="file" name="image" accept="image/png, image/jpeg" />
     
    </div>
 
</form>

</body>
</html>