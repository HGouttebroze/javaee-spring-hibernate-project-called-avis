<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="style/theme1.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>

<table>
		<thead>
			<tr>
				<th>Date d’envoi<a href="index?sort=nom">trier</a></th>
				<th>Nom du Jeu</th>
				<th>Pseudo du Joueur <a href="index?sort=genre.nom">trier</a></th>
				<th>Classification <a href="index?sort=classification">trier</a></th>
				<th>dateSortie <a href="index?sort=dateSortie">trier</a></th>
				<!--       <th>Modele economique <a href="index?sort=modeleEconomique">trier</a></th> -->
			</tr>
		</thead>
		<tbody>
			<!--   On ne parcours plus une liste mais une page @PageableDefault -->
			<c:forEach items="${jeux.content}" var="jeu">
				<%--   <c:forEach items="${jeux}" var="jeu"> --%>
				<tr>
					<td>${jeu.nom}</td>
					<td><c:forEach items="${jeu.plateformeJeu}" var="plateforme">${plateforme.nom}</c:forEach></td>
					<td>${jeu.genre.nom}</td>
					<td>${jeu.classification.nom}</td>
					<td>${jeu.dateSortie}</td>
					<%--       <td>${jeu.modeleEconomique.nom}</td> --%>

					</form>

				</tr>
			</c:forEach>
		</tbody>

	<br>
	<a href="/avis">Ajouter un avis</a>
	<br>
	<br>
	<a href="index?page=0">Accueil</a>
	<br>
	<br>
	<a href="/editeurs">Liste des Editeurs</a>
	<br>
	<br>
	<a href="index?page=0">page d'accueil</a>
<!-- 	<br> -->
<!-- 	<br> -->
<%-- 	<a href="index?page=${pageDeJeux.number-1}">Page précédente</a> --%>
<!-- 	<br> -->
<!-- 	<br> -->
<%-- 	<a href="index?page=${pageDeJeux.number+1}">Page suivante</a> --%>
	<%--     <a href="index?page=${pageDeJeux.totalPages-1}">Dernière page</a> --%>

</body>
</html>