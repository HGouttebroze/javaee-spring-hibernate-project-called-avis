<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Inscription</title>
<link href="style/theme1.css" rel="stylesheet">
</head>
<body>
	<h1>Bienvenue !</h1>
	<h2>Inscription</h2>
	<form:form modelAttribute="joueur" action="inscription" method="post"> 
 		<form:label path="pseudo">Pseudo</form:label>
 		<form:input path="pseudo" /> 
		<form:errors path="pseudo" cssClass="erreur" />
		<br>
 		<form:label path="motDePasse">Mot de passe</form:label>
		<form:password path="motDePasse" />
 		<form:errors path="motDePasse" cssClass="erreur" />
 		<br>


 		<br>
 		<form:button>Inscription</form:button>
</form:form>

	<h1>Bienvenue !</h1>
	<h2>Inscription</h2>
	<form:form modelAttribute="joueur" action="inscription"
		method="post">
		<form:label path="pseudo">Pseudo </form:label>
		<form:input path="pseudo" />
		<br>
		<form:label path="motDePasse">Mot de passe</form:label>
		<form:password path="motDePasse" />
		<br>
		<form:button>Inscription</form:button>
	</form:form>
<!-- <link href="style/theme1.css" rel="stylesheet"> -->
<!-- </head> -->
<!-- <body> -->
<!-- <h1>Bienvenue !</h1> -->
<!-- <h2>Inscription</h2> -->
<%-- <form:form modelAttribute="utilisateur" action="inscription" method="post"> --%>
<%-- <form:label path="email">Email</form:label><form:input path="email"/><form:errors path="email" cssClass="erreur" /><br> --%>
<%-- <form:label path="motDePasse">Mot de passe</form:label><form:password path="motDePasse"/><form:errors path="motDePasse" cssClass="erreur"/><br> --%>
<%-- <form:label path="">Description</form:label><form:textarea path="description"></form:textarea><form:errors path="description" cssClass="erreur"/><br> --%>
<%-- <form:label path="dateDeNaissance">Date de naissance</form:label><form:input type="date" path="dateDeNaissance"/><form:errors path="dateDeNaissance" cssClass="erreur"/><br> --%>
<%-- <form:button>Inscription</form:button> --%>
<%-- </form:form> --%>

<br>
		<a href="/connexion">connexion</a>
		<br>
		<a href="/ajouterJeu">Ajouter un jeu</a>
		<br>
		<a href="/avis">Ajouter un avis</a>
		<br>
		<a href="/editeurs">Liste des Editeurs</a>
		<br>
		<br>
		<a href="index?page=0">page d'accueil</a>
		<br>
</body>
</html>