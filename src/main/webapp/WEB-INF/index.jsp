<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="style/theme1.css" rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
	crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="entete.jsp" />
	<h1>Bienvenue</h1>
	<!-- <table class="GeneratedTable"> -->

	<form action="filtrer" method="POST">
		<label>genre</label> <select name="ID_GENRE">
			<!--             <option value="0">Tous </option> -->
			<c:forEach items="${genres}" var="genre">
				<option value="${genre.id}">${genre.nom}</option>
			</c:forEach>
		</select>
		<button type="submit" value="filtrer" class="btn btn-primary">Filtrer</button>
	</form>
	<table>
		<thead>
			<tr>
				<th>image</th>				
				<th>Nom <a href="index?sort=nom">trier</a></th>
				<th>Plateformes</th>
				<th>Genre <a href="index?sort=genre.nom">trier</a></th>
				<th>Classification <a href="index?sort=classification">trier</a></th>
				<th>dateSortie <a href="index?sort=dateSortie">trier</a></th>
				<th>Modele economique <a href="index?sort=modeleEconomique">trier</a></th>
				<th>Action</th>
				
			</tr>
		</thead>
		<tbody>
			<!--   On ne parcours plus une liste mais une page @PageableDefault -->
			<c:forEach items="${jeux.content}" var="jeu">
				<%--   <c:forEach items="${jeux}" var="jeu"> --%>
				<tr>
					<th><img src="${jeu.cheminImage}" width="80px"></th>
					<td><a href="/image?id=${jeu.id }"></a></td>
					<td>${jeu.nom}</td>
					<td><c:forEach items="${jeu.plateformeJeu}" var="plateforme">${plateforme.nom}</c:forEach></td>
					<td>${jeu.genre.nom}</td>
					<td>${jeu.classification.nom}</td>
					<td>${jeu.dateSortie}</td>
					<td>${jeu.modeleEconomique.nom}</td> 
					
					<td><br>
					<a 	class="btn btn-primary" href="image?id=${jeu.id}">Ajouter une image</a>
					<br><br> 
						<form action="supprimer" method="post"
							onsubmit="return confirm('Etes-vous sur de vouloir supprimer ${jeu.nom} ?') ? true : false;">
							<input type="hidden" name="ID_JEU" value="${jeu.id}" /> 
							<input class="btn btn-danger" type="submit" value="Supprimer" />
						</form> 
						
<%-- 					<a href="/suppression?id=${jeu.id }">Supprimer</a> --%>
						
						<br> 
						
<%-- 						<a class="btn btn-primary" href="/modification?id=${jeu.id }">Modifier</a> --%>

						<form action="modifier" method="post" onsubmit="return confirm('Etes-vous sur de vouloir modifier ${jeu.nom} ?') ? true : false;">
							<input type="hidden" name="ID_JEU" value="${jeu.id}" /> 
							<input	class="btn btn-primary" type="submit" value="Modifier" />
						</form> 					
						<br>    					
				<br> 
				<br>
				</td> 
				
				
					</form>

				</tr>
			</c:forEach>


			<br>
			<a href="/connexion">connexion</a>
			<br>
			<a href="/inscription">inscription</a>
			<br>
			<a href="/jeu">Ajouter un jeu</a>
			<br>
			<a href="/avis">Ajouter un avis</a>
			<br>
			<br>
			<a href="index?page=0">Accueil</a>
			<br>
			<br>
			<a href="/editeurs">Liste des Editeurs</a>
			<br>
			<br>
			<a href="index?page=0">page d'accueil</a>
			<br>
			<br>
			<a href="index?page=${pageDeJeux.number-1}">Page précédente</a>
			<br>
			<br>
			<a href="index?page=${pageDeJeux.number+1}">Page suivante</a>
			<%--     <a href="index?page=${pageDeJeux.totalPages-1}">Dernière page</a> --%>
</body>
</html>