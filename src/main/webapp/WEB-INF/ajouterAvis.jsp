<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="style/theme1.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>

	<h1>Ajout d'un Avis</h1>
	<form action="avis" method="post">

		<!-- 		LISTE DEROULANTE -->
		
		<label for="ID_JEUX">Jeux</label> 
			<select name="ID_JEUX">
			<option selected="selected">Veuillez choisir</option>
			<c:forEach items="${jeux}" var="jeu">
				<option value="${jeu.id}">${jeu.nom}</option>
				<br>
			</c:forEach>
		</select>
<br>		
<br>
		<!-- 			TEXT AREA -->
		<label for="DESCRIPTION">Description</label>
		<textarea name="DESCRIPTION"></textarea>
<br>
<br>
		<label for="ID_NOTE">Note attribuée</label> 
			<select name="ID_Note">
				<option selected="selected">Veuillez choisir</option>
				<!-- 				Liste de notes de 0 à 20 -->
				<c:forEach var="i" begin="0" end="20">
					<option value="${i}">${i}</option>
					<br>
				</c:forEach>
		</select>
<br>
<br>		
		<input type="submit" value="Envoyer"></select>
<!-- 		<button type="submit">Envoyer</button> -->

	</form>
<br>
<br>	
	<a href="/editeurs">Liste des Editeurs</a>
<br>
<br>
	<a href="index?page=0">Accueil</a>
</body>
</html>