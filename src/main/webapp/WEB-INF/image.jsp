<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ajout d'une image</title>
</head>
<body>
	<h1>Ajoutez une image</h1>

	<form action="image?id=${jeu.id}" method="post" enctype="multipart/form-data">
		
		<input type="file" name="image" accept="image/png, image/jpeg"><br>
		<button type="submit">Ajouter</button>
	</form>
</body>
</html>