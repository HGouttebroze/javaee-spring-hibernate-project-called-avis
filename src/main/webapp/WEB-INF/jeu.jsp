<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="style/theme1.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>

	<h1>Ajouter un Jeu</h1>
	<form:form modelAttribute="jeu" action="jeu" method="post">


		<tr>
			<td>Nom du jeux</td>

			<td><form:input path="nom" /></td>

			<td><form:errors path="nom" cssClass="erreur"></form:errors></td>
		</tr>
		<br>
		<br>
		<form:label path="description">Description</form:label>
		<form:textarea path="description" />
		<form:errors path="description" cssClass="erreur" />
		<br>
		<br>
		
		<br>
		
		<br>
		<br>
		<tr>
		<td><form:label path="modeleEconomique"></form:label></td>
		<td>	<form:select path="genre">
		<form:option value="">Veuillez choisir un modele economique</form:option>
        <form:options items="${modeleEconomiques}" itemValue="id" itemLabel="nom"></form:options>
         </form:select></td>
            <td><form:errors path="genre" cssClass="erreur" /></td>
		<tr>
		
		<br>
		<td><form:select path="editeur">
					<form:option value="">Editeurs</form:option>
					<form:options items="${editeurs}" itemValue="id" itemLabel="nom" />
				</form:select></td>
			<td><form:errors path="editeur" cssClass="erreur"></form:errors></></td>
		
		<tr>
		<td><form:label path="genre"></form:label></td>
		<td>	<form:select path="genre">
		<form:option value="">Veuillez choisir un genre</form:option>
        <form:options items="${genres}" itemValue="id" itemLabel="nom"></form:options>
         </form:select></td>
            <td><form:errors path="genre" cssClass="erreur" /></td>
		</tr>

<!-- 		<tr> -->
<%-- 		<td><form:label path="plateforme"></form:label></td> --%>
<%-- 		<td>	<form:select path="plateforme"> --%>
<%-- 		<form:option value="">Veuillez choisir une plateforme</form:option> --%>
<%--         <form:options items="${plateformeJeu}" itemValue="id" itemLabel="nom"></form:options> --%>
<%--          </form:select></td> --%>
<%--             <td><form:errors path="plateforme" cssClass="erreur" /></td> --%>
<!-- 		<tr> -->
				<%-- 			<td><form:select path="jeu" items="${jeu}" /></td> --%>
			</tr>
			<%-- 				<form:label path="email">Email</form:label> --%>
			<%-- 				<form:input path="email" /> --%>
			<%-- 				<form:errors path="email" cssClass="erreur" /> --%>
			<!-- 				<br> -->
			<%-- 				<form:label path="motDePasse">Mot de --%>
			<%-- 		passe</form:label> --%>
			<%-- 				<form:password path="motDePasse" /> --%>
			<%-- 				<form:errors path="motDePasse" cssClass="erreur" /> --%>
			<!-- 				<br> -->
			<%-- 				<form:label path="description">Description</form:label> --%>
			<%-- 				<form:textarea path="description" /> --%>
			<%-- 				<form:errors path="description" cssClass="erreur" /> --%>
			<!-- 				<br> -->
			<%-- 				<form:label path="ville">Ville</form:label> --%>
			<%-- 				<form:select path="ville"> --%>
			<%-- 					<form:option value="">Merci de choisir une --%>
			<%-- 		ville</form:option> --%>
			<%-- 					<form:options items="${villes}" itemValue="id" itemLabel="nom"></form:options> --%>
			<%-- 				</form:select> --%>

			<%-- 				<form:errors path="ville" cssClass="erreur" /> --%>
		</tbody>

<%-- 		<form:button>Envoyer</form:button> --%>
		
<!-- 		boutton avec Bootsrap 5 -->
		 <button type="submit" class="btn btn-primary">Envoyer</button>
		 
		</table>
	</form:form>





	<br>
	<a href="/connexion">connexion</a>
	<br>
	<a href="/inscription">inscription</a>
	<br>
	<a href="/">Accueil</a>
	<br>
	<a href="/editeur">Liste d'éditeurs</a>
</body>
</html>